libreturn-multilevel-perl (0.08-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.08.
  * Update years of upstream copyright.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 Dec 2021 16:25:03 +0100

libreturn-multilevel-perl (0.06-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libscope-upper-perl.
    + libreturn-multilevel-perl: Drop versioned constraint on
      libscope-upper-perl in Recommends.
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.06.
  * Update Upstream-Contact.
  * Update debian/upstream/metadata.
  * Drop Test-Fatal-0.016.patch, fixed upstream.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Oct 2021 19:03:01 +0200

libreturn-multilevel-perl (0.05-2) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Add patch from upstream pull request for Test-Fatal 0.016
    compatibility. Thanks to ci.debian.net.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Aug 2020 17:25:35 +0200

libreturn-multilevel-perl (0.05-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Florian Schlichting ]
  * Import upstream version 0.05
  * Update upstream metadata
  * Recommend libscope-upper-perl
  * Update copyright years
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 4.1.1

 -- Florian Schlichting <fsfs@debian.org>  Sun, 08 Oct 2017 22:43:41 +0200

libreturn-multilevel-perl (0.04-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 0.04
  * Add years of upstream copyright.
  * Update (build) dependencies.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Oct 2014 20:58:26 +0200

libreturn-multilevel-perl (0.03-1) unstable; urgency=low

  * Initial Release. (Closes: #719421)

 -- Florian Schlichting <fsfs@debian.org>  Sun, 11 Aug 2013 16:26:47 +0200
